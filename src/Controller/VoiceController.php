<?php

namespace Drupal\voice_search_redirect\Controller;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\OpenModalDialogCommand;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Controller\ControllerBase;

/**
 * Provides functionality for voice search.
 */
class VoiceController extends ControllerBase {

  /**
   * Return a modal for voice search.
   */
  public function popUp() {
    $options = [
      'dialogClass' => 'popup-dialog-class',
      'width' => '50%',
    ];
    $response = new AjaxResponse();
    $response->addCommand(new OpenModalDialogCommand(t('Voice Command'), t('<html>
    	<div class="img_container"> <img src="//i.imgur.com/cHidSVu.gif" style ="display:block;
    margin:auto;"> </div> <br>
    	</html>'), $options));
    $addClassCommand = new InvokeCommand('.popup-dialog-class', 'addClass', ['close_popup']);
    $response->addCommand($addClassCommand);
    return $response;
  }

}
