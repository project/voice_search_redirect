<?php

namespace Drupal\voice_search_redirect\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure the voice command settings.
 */
class VoiceCommandSettingsForm extends ConfigFormBase {
  /**
   * Voice search config name.
   *
   * @var string Config settings
   */
  const SETTINGS = 'voice_search_redirect.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'voice_search_redirect_admin_setting';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);

    $name_field = $form_state->get('num_of_commands');
    $form['#tree'] = TRUE;

    $form['voice_commands_fieldset'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Custom Voice Command'),
      '#prefix' => "<div id='voice-command-fieldset-wrapper'>",
      '#suffix' => '</div>',
    ];

    $voices = \Drupal::configFactory()->get('voice_search_redirect.settings')->get('commands');
    $links = \Drupal::configFactory()->get('voice_search_redirect.settings')->get('links');

    if (empty($name_field)) {
      $name_field = $form_state->set('num_of_commands', count($voices));
    }

    for ($i = 1; $i <= $form_state->get('num_of_commands'); $i++) {
      $form['voice_commands_fieldset'][$i]['voices'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Enter Voice Command'),
        '#default_value' => $voices[$i],

        '#maxlength' => 64,
        '#size' => 64,
        '#prefix' => "<div class='inner-fieldset'><legend><span class='fieldset-legend'>Custom Voice Command {$i}</span></legend>",
      ];
      $form['voice_commands_fieldset'][$i]['urls'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Enter Url'),
        '#default_value' => $links[$i],

        '#maxlength' => 124,
        '#size' => 124,
      ];

    }
    $form['voice_commands_fieldset']['actions'] = [
      '#type' => 'actions',
    ];
    $form['voice_commands_fieldset']['actions']['add_name'] = [
      '#type' => 'submit',
      '#value' => t('Add one more'),
      '#submit' => ['::addOne'],
      '#ajax' => [
        'callback' => '::addmoreCallback',
        'wrapper' => "voice-command-fieldset-wrapper",
      ],
    ];
    if ($form_state->get('num_of_commands') > 1) {
      $form['voice_commands_fieldset']['actions']['remove_name'] = [
        '#type' => 'submit',
        '#value' => t('Remove one'),
        '#submit' => ['::removeCallback'],
        '#ajax' => [
          'callback' => '::addmoreCallback',
          'wrapper' => "voice-command-fieldset-wrapper",
        ],
      ];
    }
    $form_state->setCached(FALSE);

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => t('Submit'),
    ];

    return $form;
  }

  /**
   * Callback for both ajax-enabled buttons.
   *
   * Selects and returns the fieldset with the names in it.
   */
  public function addmoreCallback(array &$form, FormStateInterface $form_state) {
    $name_field = $form_state->get('num_of_commands');
    return $form['voice_commands_fieldset'];
  }

  /**
   * Submit handler for the "add-one-more" button.
   *
   * Increments the max counter and causes a rebuild.
   */
  public function addOne(array &$form, FormStateInterface $form_state) {
    $name_field = $form_state->get('num_of_commands');
    $add_button = $name_field + 1;
    $form_state->set('num_of_commands', $add_button);
    $form_state->setRebuild();
  }

  /**
   * Submit handler for the "remove one" button.
   *
   * Decrements the max counter and causes a form rebuild.
   */
  public function removeCallback(array &$form, FormStateInterface $form_state) {
    $name_field = $form_state->get('num_of_commands');
    if ($name_field > 1) {
      $remove_button = $name_field - 1;
      $form_state->set('num_of_commands', $remove_button);
    }
    $form_state->setRebuild();
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues(['voice_commands_fieldset']);
    $no_of_commands = $form_state->get('num_of_commands');
    $links = $values['voice_commands_fieldset'][1]['links'];

    for ($i = 1; $i <= $form_state->get('num_of_commands'); $i++) {
      $voices[$i] = $values['voice_commands_fieldset'][$i]['voices'];
      $links[$i] = $values['voice_commands_fieldset'][$i]['urls'];
    }

    $this->configFactory->getEditable(static::SETTINGS)
      ->set('commands', $voices)
      ->save();
    // Passing urls array.
    $this->configFactory->getEditable(static::SETTINGS)
      ->set('links', $links)
      ->save();

    parent::submitForm($form, $form_state);

  }

}
