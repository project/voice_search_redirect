CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

This is a Voice Search Redirect Module.

 * It permits users to create voice command for particular path of your site.

 * After defining voice command and path, you can navigate through your site by
   voice command.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/voice_search_redirect

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/voice_search_redirect


INSTALLATION
------------

 * Install the Voice Search Redirect module as you would normally install a
   contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

 * After installing the module, navigate to :-
   Administration > Configuration > Development > Custom Voice Command

 * This module contains a block which you have to place by Structure > Block Layout.

 * You will be able to see the config form where you can add/edit voice command and
   redirect path respectively.

 * User can also delete the voice commands if it is not required anymore.


MAINTAINERS
-----------

 * Akash Kumar (AkashkumarOSL) - https://www.drupal.org/u/akashkumarosl

Supporting organizations:

 * OpenSense Labs - https://www.drupal.org/opensense-labs
