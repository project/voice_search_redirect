/**
 * @file
 * Javascript functions.
 */

(function ($, Drupal, drupalSettings) {

  Drupal.behaviors.voice_search_redirect = {
  attach: function (context, settings) {
  var y = document.getElementById("voice_command");
  function annyang_voice(){
    var voices = drupalSettings.voice;
    var links = drupalSettings.link;
    window.commands = {
            'Hello': function () {
                alert('Hi! I can hear you.');
            },
            'Reload': function () {
                location.reload();
            },
    };
    function passurl(key) {
      return function () {
      window.location = window.drupalSettings.link[key];
      }
    }
    for (key in window.drupalSettings.voice) {
      window.commands[drupalSettings.voice[key]] = passurl(key);
    }
    if (annyang) {
      annyang.addCommands(window.commands);
      annyang.start();
    }

}
   // Close button.
   y.addEventListener('click', annyang_voice);
    setTimeout(function () {
      var close = document.querySelector(".popup-dialog-class .ui-button-icon-only");
    function annyang_close() {
      if (annyang) {
      annyang.abort();
      }
    }
    close.addEventListener('click', annyang_close);
  },500);
}
};

})(jQuery, Drupal, drupalSettings);
